#!/bin/bash
if git --version; then
    echo "[SUCCESS] Git installation completed"
else
    echo "[WARNING] Git installation failed"
fi

if curl 127.0.0.1; then
    echo "[SUCCESS] curl installation completed"
else
    echo "[WARNING] curl installation failed"
fi

if grep jordan etc/passwd; then
    echo "[SUCCESS] jordan has been created"
else
    echo "[WARNING] jordan does not exist"
fi

if grep jonathan etc/passwd; then
    echo "[SUCCESS] jonathan has been created"
else
    echo "[WARNING] jonathan does not exist"
fi

if python3 --version; then
    echo "[SUCCESS] Python3 installation completed"
else
    echo "[WARNING] Python3 installation failed"
fi

founded = curl 127.0.0.1 | grep -c "Mozilla is cool"
if $founded == 1; then
    echo "[SUCCESS] curl managed to retrieve the right webpage"
else
    echo "[WARNING] curl couldn't find the right webpage"
fi