FROM debian:stretch

RUN apt-get update && apt-get install -y --no-install-recommends \
    curl=7.52.1-5+deb9u12 \
    nginx=1.10.3-1+deb9u5 \
    git=1:2.11.0-3+deb9u7 \
    python3=3.5.3-1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN addgroup esgi

RUN useradd -rm -d /home/jonathan -s /bin/bash -g esgi -u 1001 jonathan

RUN useradd -rm -d /home/jordan -s /bin/bash -g esgi -u 1002 jordan

RUN useradd -d /home/dockle -m -s /bin/bash dockle

USER dockle

COPY . .

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
